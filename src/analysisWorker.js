import * as Comlink from "comlink";
import {Analysis} from "./analysis";

function getProba(state) {
  try {
    const [, proba_fork, fork_length, sum_all_proba] = Analysis.getProba(
      parseFloat(state.nb_samples),
      parseFloat(state.te),
      parseFloat(state.dm),
      parseFloat(state.dp),
      parseFloat(state.de),
      parseFloat(state.stake),
      parseFloat(state.target_probability),
    );

    return {
      proba_fork,
      fork_length,
      sum_all_proba,
      errorMessage: null,
    };
  } catch (error) {
    return {
      ...state.results,
      errorMessage: JSON.stringify(error, null, 2),
    };
  }
}

Comlink.expose({getProba});
