/* eslint-disable import/analysis */
import {Analysis} from "./analysis";

export function getProba(state) {
  const [, proba_fork, fork_length, sum_all_proba] = Analysis.getProba(
    parseFloat(state.nb_samples),
    parseFloat(state.te),
    parseFloat(state.dm),
    parseFloat(state.dp),
    parseFloat(state.de),
    parseFloat(state.stake),
    parseFloat(state.target_probability),
  );

  return {proba_fork, fork_length, sum_all_proba};
}
