import * as Comlink from "comlink";
/* eslint-disable import/no-webpack-loader-syntax */
import Worker from 'worker-loader!./analysisWorker.js';

const worker = Comlink.wrap(new Worker());

export async function getProba(state) {
  return await worker.getProba(state);
}
